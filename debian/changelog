libstring-tagged-perl (0.24-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.24.

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Jul 2024 16:56:05 +0200

libstring-tagged-perl (0.23-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.23.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 10 Jun 2024 18:54:54 +0200

libstring-tagged-perl (0.22-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.22.

 -- gregor herrmann <gregoa@debian.org>  Wed, 04 Oct 2023 21:10:56 +0200

libstring-tagged-perl (0.21-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.21.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Sep 2023 02:14:50 +0200

libstring-tagged-perl (0.20-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.20.

 -- gregor herrmann <gregoa@debian.org>  Thu, 16 Feb 2023 18:55:02 +0100

libstring-tagged-perl (0.19-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.19.
  * Update years of upstream copyright.
  * Update test dependencies.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Tue, 07 Feb 2023 21:46:43 +0100

libstring-tagged-perl (0.18-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libstring-tagged-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 00:14:28 +0100

libstring-tagged-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Import upstream version 0.18.
  * Update years of upstream copyright.
  * Update long description from upstream README.

  [ Étienne Mollier ]
  * fix-barewords.patch added: this works around a "Bareword" issue occurring
    during syntax check in autopkgtest.
  * d/control: declare conformance to standards level 4.6.1.

 -- Étienne Mollier <emollier@debian.org>  Wed, 25 May 2022 20:39:01 +0200

libstring-tagged-perl (0.17-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use canonical URL in Vcs-Git.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.17.
  * Drop spelling.patch, applied upstream.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Sep 2021 16:43:03 +0200

libstring-tagged-perl (0.16-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.16
  * Update d/copyright years
  * Bump debhelper-compat to 12
  * Bump Standards-Version to 4.4.0
  * Update d/upstream/metadata
  * Add patch to fix spelling

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Fri, 26 Jul 2019 09:28:10 +0530

libstring-tagged-perl (0.15-1) unstable; urgency=low

  * Initial release.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 2 Jul 2018 14:05:36 +0200
